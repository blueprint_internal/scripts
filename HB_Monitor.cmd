@echo off
setlocal EnableDelayedExpansion
SET DBPath=D:\Dropbox (Facebook)\Blueprint - Localization
SET scriptPath=D:\scripts\blueprint\scripts

::Get today's date
FOR /f "tokens=1-2" %%a in ("%date%") do (
  FOR /f "tokens=1-3 delims=/" %%b in ("%%b") do SET today=%%d-%%b-%%c
)

::Get time of handback
for /f "tokens=1-2 delims=:" %%a in ("%time%") do (
  IF %%a lss 10 (
    set hour=%%a
    set now=!hour: =!-%%b
  ) else (
    set now=%%a-%%b
  )
)

SET logfile=HB_log_%today%.txt

IF exist %scriptPath%\HB_log_%today%.txt (
  
  ::Run against a clean list
  IF EXIST "%ScriptPath%\todaysHB.txt" del "%ScriptPath%\todaysHB.txt"
  
  ::Create a HB list.
  powershell -file "%ScriptPath%\ps1\Create_HB_List.ps1" "%scriptPath%\HB_log_%today%.txt"

  IF NOT EXIST "%DBPath%\Handoff\History" (
  	md "%DBPath%\Handoff\History"
    
  	move /y "%scriptPath%\HB_log_%today%.txt" "%DBPath%\Handoff\History\HB_log_%today%_%now%.txt"
    call %scriptPath%\run.cmd build "%ScriptPath%\todaysHB.txt"
    move /y "%scriptPath%\todaysHB.txt" "%DBPath%\Handoff\History\HB_List_%today%_%now%.txt"
    
  ) else (
    move /y "%scriptPath%\HB_log_%today%.txt" "%DBPath%\Handoff\History\HB_log_%today%_%now%.txt"
    call %scriptPath%\run.cmd build "%ScriptPath%\todaysHB.txt"
    move /y "%scriptPath%\todaysHB.txt" "%DBPath%\Handoff\History\HB_List_%today%_%now%.txt"
  )
) else (
  %scriptPath%\bin\xecho /s "%scriptPath%\HB_log_%today%.txt not available" /n
)