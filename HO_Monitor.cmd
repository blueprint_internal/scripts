@echo off
SETLOCAL EnableDelayedExpansion
SET DBPath=D:\Dropbox (Facebook)\Blueprint - Localization
SET scriptPath=D:\scripts\blueprint\scripts

::Get today's date
FOR /f "tokens=1-2" %%a in ("%date%") do (
  FOR /f "tokens=1-3 delims=/" %%b in ("%%b") do set today=%%d-%%b-%%c
)

::Get time of handback
for /f "tokens=1-2 delims=:" %%a in ("%time%") do (
  IF %%a lss 10 (
    set hour=%%a
    set now=!hour: =!-%%b
  ) else (
    set now=%%a-%%b
  )
)

IF exist %scriptPath%\HO_log_%today%.txt (
  call %scriptPath%\run.cmd handoff
  IF NOT EXIST "%DBPath%\Handoff\History" (
  	md "%DBPath%\Handoff\History"
  	move %scriptPath%\HO_log_%today%.txt "%DBPath%\Handoff\history\HO_log_%today%_%now%.txt"
  ) else (
  	move %scriptPath%\HO_log_%today%.txt "%DBPath%\Handoff\history\HO_log_%today%_%now%.txt"
  )
) else (
  %scriptPath%\bin\xecho /s "%scriptPath%\HO_log_%today%.txt not available" /n
)