﻿<# $Processtype="build"
$inputfile="D:\scripts\Blueprint\scripts\history\todayshb_2017-05-02_7-55.txt"
$buildFolder= "D:\Dropbox (Facebook)\Blueprint - Localization\Handoff\Builds" #>
$Processtype=$args[0]
$inputfile=$args[1]
$buildFolder= $env:buildFolder
$datestamp = get-date -uformat "%Y-%m-%d"
$lists=get-content $inputfile


function writehtml ([string]$handback_lists, [string]$html_heading, [string]$type)
{
    $html_style = "<style>
                    body { background-color:#FFFFFF;
                    font-family:Tahoma;
                    font-size:12pt; }
                    td, th { border:1px solid black;
                    border-collapse:collapse; }
                    th { color:white;
                    background-color:black; }
                    table, tr, td, th { padding: 2px; margin: 0px }
                    table { width:95%;margin-left:5px; margin-bottom:20px;}
                    </style><br>"
    $html_body = $html_style + "`n" + $html_heading
    foreach ($list in $handback_lists.Split(" "))
    {
        if ($list -match "\\")
        {
            $temp = $list.Split("\")
            $lang = $temp[0]
            $course = $temp[1]
            if ($temp.Length -eq 3) {
                
                $commit = $temp[2]
                $html_body += "<tr><td>" + $lang + "</td><td><a href=""https://bitbucket.org/blueprint_internal/" + 
                            $course + "_" + $lang.ToLower() + """ >" + $course + "_" + $lang.ToLower() + 
                            "</a></td><td>" + $commit +"</td></tr>"
            }
            else {
                
                $html_body += "<tr><td>" + $lang + "</td><td><a href=""https://www.dropbox.com/work/Blueprint%20-%20Localization/Handoff/Builds/" + $course+ "/" + $lang + """ >" + $course + "</a></td><td>" + $course + "_" + $lang + ".zip</td></tr>"
            }
        }
        else
        {
            $course =$list
            $html_body += "<tr><td>en_US</td><td><a href=""https://www.dropbox.com/work/Blueprint%20-%20Localization/Handoff/Moravia/"" >" + $course + "</a></td><td>" + $course + ".zip</td></tr>"
        }
    }
    $html_body += "</table>"
    return $html_body
}

switch ($Processtype)
{
    HANDOFF {
        $heading = "<table><tr><th>Language(s)</th><th>Course(s)</th><th>File Name</th></tr>"
        $html = writehtml $lists $heading $Processtype
        $body="<H1>Handoff(s) available: </H1>" + $html
        $recipient = "blueprint_external@fb.com"
        $cc = "vincentedd@fb.com"
    }
    HANDBACK {
        $heading = "<table><tr><th>Language(s)</th><th>Course(s)</th><th>File Name</th></tr>"
        $html = writehtml $lists $heading $Processtype
        $body="<H1>Handback(s) available: </H1>" + $html
        $recipient = "blueprint_external@fb.com"
        $cc = "pilyi@fb.com"
    }
    BUILD {
        $heading = "<table><tr><th>Language(s)</th><th>Course(s)</th><th>File Name</th></tr>"
        $html = writehtml $lists $heading $Processtype
        $body="<H1>Build(s) available: </H1>" + $html
        $recipient = "blueprint_external@fb.com"
        $cc = "pilyi@fb.com"
    }
    SUBMIT {
        $heading = "<table><tr><th>Language(s)</th><th>Course(s)</th><th>Committed ID(s)</th></tr>"
        $html = writehtml $lists $heading $Processtype
        $body="<H1>Commit(s) available: </H1>" + $html
        $recipient = ("ericalow@fb.com", "leejessica@fb.com", "sherinelau@fb.com", "daisytam@fb.com", "pprisel@fb.com")
        $cc = ("blueprint_external@fb.com" , "vincentedd@fb.com")
    }
    default {
        "Not a valid type"
    }
}
#to fix remote certificate issue
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { return $true }

#Send-MailMessage -From "utl-i18n-mailer@fb.com" -to "blueprint_external@fb.com" -Subject "[$datestamp] Blueprint $Processtype Report" -Body "$body" -SmtpServer "mail.thefacebook.com" -UseSsl -BodyAsHtml
Send-MailMessage -From "utl-i18n-mailer@fb.com" -to $recipient -cc $cc -Subject "[$datestamp] Blueprint $Processtype Report" -Body "$body" -SmtpServer "mail.thefacebook.com" -UseSsl -BodyAsHtml
