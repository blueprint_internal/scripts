﻿$logPath = $env:ScriptPath
$logFile = $args[0]
$handbacklist = "todayshb.txt"

#Create build list...
#Get-Content $logPath\$inputFile | Get-Unique > "$logPath\todaysHB.txt"

$file=get-content $logFile

$arrays= @()

Foreach ($line in $file) {
    $fields = $line.Split("\")
    $key = $fields[0] + "\" + $fields[1]
    if ($arrays.Contains($key) -eq $false) {$arrays += $key}
}

Foreach ($array in $arrays) {
    $array | Out-File -append "$logPath\$handbacklist"
}