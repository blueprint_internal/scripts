$bitbucket_source = "bitbucket.org"
$bitbucket_login = $env:GitUser
$bitbucket_password = $env:GitPW
$DBPath = $env:DBPath
$List_file = $args[0]
$ScriptPath = $env:ScriptPath
$pair = "$($bitbucket_login):$($bitbucket_password)"
$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$basicAuthValue = "Basic $encodedCreds"
$commits = ""
$comments = $env:comments -replace "`"", ""

$headers = @{
    Authorization = $basicAuthValue
}

#Remove existing log from the previous submit
if (Test-Path "$ScriptPath\Committed.log") {
    Remove-Item -Path "$ScriptPath\Committed.log"
}

$submit_lists = Get-Content $List_file

foreach ($list in $submit_lists)
{
    $temp = $list.Split("\")
    $lang = $temp[0]
    $course = $temp[1].ToLower()
    if ($lang.ToLower() -ne "en-us")
    {
        $repository_name = "$($course)_$($lang)".ToLower()
        try
        {
            Invoke-WebRequest -Uri "https://api.bitbucket.org/2.0/repositories/blueprint_internal/$repository_name" | Out-Null
        }
        catch
        {
            $invoke_error = $_.Exception.Response.StatusCode.Value__
            if ($invoke_error -eq 404)
            {
                Write-Host -NoNewline "`nCreating repository " -ForegroundColor Yellow
                Write-Host -NoNewline $repository_name -ForegroundColor Blue -BackgroundColor Gray
                Invoke-WebRequest -Uri "https://api.bitbucket.org/2.0/repositories/blueprint_internal/$repository_name" -Headers $headers -Method Post | Out-Null
                Write-Host "`n`tdone"
            }
            else {
                Write-Host "`n`t$repository_name : Already exist!"
            }
        }
        
        $temp_folder = Join-Path ([System.IO.Path]::GetTempPath()) ([System.Guid]::NewGuid())
        New-Item -ItemType Directory -Path $temp_folder | Out-Null
            
        #$git_args = "clone", "https://$($bitbucket_login):$($bitbucket_password)@$($bitbucket_source)/blueprint_internal/$repository_name.git", """$temp_folder"""
        $git_args = "clone", "https://$($bitbucket_login)@$($bitbucket_source)/blueprint_internal/$repository_name.git", """$temp_folder"""
        Write-Host -NoNewline "`nCloning folder " -ForegroundColor Yellow
        Write-Host -NoNewline $repository_name -ForegroundColor Blue -BackgroundColor Gray
        Start-Process -FilePath "git" -ArgumentList $git_args -Wait
        Write-Host "`n`tdone"

        Start-Process -FilePath "robocopy" -ArgumentList "/E", """$DBPath\Handoff\Builds\$course\$lang""", """$($temp_folder)""" -Wait

        $git_args = "add", "*"
        Start-Process -FilePath "git" -ArgumentList $git_args -WorkingDirectory $temp_folder -Wait

        $git_args = "commit", "-m", """$comments"""
        Start-Process -FilePath "git" -ArgumentList $git_args -WorkingDirectory $temp_folder -Wait

        $commit = & "git" -C $temp_folder rev-parse --short HEAD
        "$lang\$course\$commit" | Out-File -Append "$ScriptPath\Committed.log"
        
        $git_args = "push", "-u", "origin", "master"
        Start-Process -FilePath "git" -ArgumentList $git_args -WorkingDirectory $temp_folder -Wait
    }

    Remove-Item -Path $temp_folder -Recurse -Force
}
