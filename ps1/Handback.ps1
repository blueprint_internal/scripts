﻿$handback_folder = "$env:DBPath\Handoff\Delivered"
$builds_folder = "$env:DBPath\Handoff\Builds"
$archive_folder = "$env:DBPath\Handoff\Archived\Received"


foreach ($lang in Get-ChildItem $handback_folder)
{
    Write-Host -NoNewline "Processing $lang for " -ForegroundColor Yellow
    
    foreach ($project in Get-ChildItem $lang.FullName)
    {
        Write-Host "$project" -ForegroundColor Blue -BackgroundColor Gray

        Write-Host -NoNewline "`tcopying ""$($project.FullName)"" to ""$builds_folder\$project\$lang\en""..."
        Get-ChildItem "$($project.FullName)" | Copy-Item -Destination "$builds_folder\$project\$lang\en" -Recurse -Force -Container
        if (Test-Path "$archive_folder\$project\$lang")
        {
            Remove-Item "$archive_folder\$project\$lang" -Force -Recurse
        }
        New-Item -Type Directory -Path "$archive_folder\$project\$lang\en" | Out-Null
        Get-ChildItem "$($project.FullName)" | Copy-Item -Destination "$archive_folder\$project\$lang\en" -Recurse -Force -Container
        Remove-Item $project.FullName -Recurse -Force
        Write-host "Done"
    }
    Remove-Item $lang.FullName -Recurse -Force
    Write-Host "`n"
}