﻿$folder='D:\Dropbox (Facebook)\Blueprint - Localization\Handoff\Archived\Received'
$filter='*.json'

# In the following line, you can change 'IncludeSubdirectories to $true if required. 
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $true;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}

Register-ObjectEvent $fsw Changed -SourceIdentifier FileChanged -Action {
$logfilepath="d:\Scripts\Blueprint\scripts"
$path = $Event.SourceEventArgs.FullPath
$changeType = $Event.SourceEventArgs.ChangeType
$timeStamp = $Event.TimeGenerated
$datestamp = get-date -uformat "%Y-%m-%d"
$Body = " $path was $changeType at $timeStamp"
$PIECES=$path.split("\")
$lang=$PIECES[-3]
$RepoName=$PIECES[-4]
$filename=$PIECES[-1]
$log = "HB_log_$datestamp.txt"
Out-File -FilePath $logfilepath\$log -Append -InputObject "$lang\$RepoName\$filename was $changeType at $timeStamp"
#Write-host "$lang\$RepoName\$filename was $changeType at $timeStamp">>$logfilepath\$log
}
# To stop the monitoring, run the following commands:
# Unregister-Event FileChanged

