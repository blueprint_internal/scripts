﻿$templateURL = "adapt_template_2-0"
$templatelocal =$env:TemplatePath
$languages = $env:language
$RepoName = $env:RepoName -replace '\s', ''
$scriptPath = $env:scriptPath
$zipFileName = $RepoName + "_" + "$env:language.zip"
$buildFolder = "$env:TemplatePath\Build"
$HandoffPath = "$env:DBPath\Handoff\Builds\$RepoName"
$HandbackPath = "$env:DBPath\Handoff\Archived\Received\$RepoName"
$tempPath = $env:TempPath

Function copyfiles([string]$from, [string]$to, [string]$language)
{
    if (!($lcid="en_US"))
    {
        Write-host -NoNewline "`t`tcopying localized JSON files to en folder under template folder..."
        Start-Process -FilePath "robocopy" -ArgumentList "/copyall", """$from\en""", """$to\en""" -Wait
        Write-Host "Done"
    }
    else 
    {
        #Copy en_US files to template folder
        Write-Host -NoNewline "`t`tcopying ""$HandoffPath\En-US\en"" to ""$to\en"" folder ..."
        Start-Process -FilePath "robocopy" -ArgumentList "/MIR", """$HandoffPath\En-US\en""", """$to\en""" -Wait
        Write-Host "Done"

        #Copy localized json files to template folder
        Write-Host -NoNewline "`t`tcopying localized json files from ""$from\$language\en"" to ""$to\en"" folder ..."
        Start-Process -FilePath "robocopy" -ArgumentList "/copyall", """$from\$language\en""", """$to\en""" -Wait
        Write-Host "Done"
    }

}

Write-Host -NoNewline "Building " -ForegroundColor Yellow
Write-Host $RepoName -ForegroundColor Blue -BackgroundColor Gray

if (!(Test-Path "$templatelocal\src\course\en")) 
{
    New-item -ItemType directory -Path "$templatelocal\src\course\en" | Out-Null
    #Copy localized json files
    Write-Host -NoNewline "`tCreating build folder..."
    copyfiles "$HandbackPath" "$templatelocal\src\course" "$languages"
    Write-Host "Done"
}
else
{
    Write-Host -NoNewline "`tRemoving existing ""$templatelocal\src\course\en"" folder..."
    Remove-Item "$templatelocal\src\course\en" -Force -Recurse
    Write-Host "Done"
    New-item -ItemType directory -Path "$templatelocal\src\course\en" | Out-Null
    #Copy localized json files
    Write-Host "`tCreating build folder..."
    copyfiles "$HandbackPath" "$templatelocal\src\course" "$languages"
}

#Execute build command
Write-host -NoNewline "`tBuilding a localized build for $RepoName..."
Start-Process -NoNewWindow -FilePath "grunt" -ArgumentList "build" -Wait
Write-Host "Done"

# Load assembly for Zip operations
Add-Type -assembly "system.io.compression.filesystem"


if (Test-Path "$templatelocal\build")
{
    if (!(Test-Path "$TempPath\$zipFileName"))
    {
        #Enable RTL for ar_AR
        if ($languages.ToLower() -eq "ar_ar")
        {
            #Replace "_defaultDirection":"ltr" to "rtl"
           (get-content $buildFolder\course\config.json).Replace("ltr", "rtl") | Out-File $buildFolder\course\config.json
        }

        #Create a zip file
        [System.IO.Compression.ZipFile]::CreateFromDirectory("$buildFolder", "$TempPath\$zipFileName")

        #Move files to Blueprint localization Dropbox location
        Write-Host -NoNewline "`tMoving $TempPath\$zipFileName to $HandoffPath\$languages..."
        Move-Item -Path "$TempPath\$zipFileName" -Destination "$HandoffPath\$languages" -Force
        Write-host "done`n"
    }
}
else
{
    Write-host "`tError: $templatelocal\build does not exist" -ForegroundColor Red
    Write-host "`tTry installing the template first then run build again"
}