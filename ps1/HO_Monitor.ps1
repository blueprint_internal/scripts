﻿$folder='D:\Dropbox (Facebook)\Blueprint - Localization\Handoff\RepoNames'
$filter='*.txt'

# In the following line, you can change 'IncludeSubdirectories to $true if required. 
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $true;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}

Register-ObjectEvent $fsw Changed -SourceIdentifier RepoNames -Action {
$logfilepath="d:\Scripts\Blueprint\scripts"
$path = $Event.SourceEventArgs.FullPath
$changeType = $Event.SourceEventArgs.ChangeType
$timeStamp = $Event.TimeGenerated
$datestamp = get-date -uformat "%Y-%m-%d"
$Body = " $path was $changeType at $timeStamp"
$RepoName=Get-Content 'D:\Dropbox (Facebook)\Blueprint - Localization\Handoff\RepoNames\RepoNames.txt'
$log = "HO_log_$datestamp.txt"
Out-File -FilePath $logfilepath\$log -Append -InputObject "$RepoName was added at $timeStamp"
#Write-host "$lang\$RepoName\$filename was $changeType at $timeStamp">>$logfilepath\$log
}
# To stop the monitoring, run the following commands:
# Unregister-Event RepoNames

