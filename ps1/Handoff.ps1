﻿$bitbucket_source = "bitbucket.org/blueprint_internal"
$bitbucket_login = $env:GitUser
$bitbucket_password = $env:GitPW
$courses_file="$env:DBPath\Handoff\RepoNames\RepoNames.txt"
$local_root = "$env:DBPath\Handoff\Builds"
$templateURL = "adapt_template_2-0"
$templatelocal = "$env:DBPath\Handoff\adapt_template_2-0"
$languages = $env:language
$handoff_folder = "$env:DBPath\Handoff\JSON_HO"
$archive_folder = "$env:DBPath\Handoff\Archived\Sent"
$json_files = "components.json", "contentObjects.json", "course.json"
$sourceFolder = "$env:DBPath\Handoff\JSON_HO"
$handoffFolder = "$env:DBPath\Handoff\Moravia"

#$bitbucket_login = [System.Web.HttpUtility]::UrlEncode($bitbucket_login)
$courses = Get-Content $courses_file
if (!(Test-Path $templatelocal))
{
    Remove-Item $templatelocal -Force -Recurse

    New-Item -Type Directory -Path $templatelocal | Out-Null
        $git_args = "clone", "https://$($bitbucket_login):$($bitbucket_password)@$($bitbucket_source)/$($templateURL).git", """$templatelocal"""

    Write-Host -NoNewline "Cloning template from BucketSource..." -ForegroundColor Yellow
    Start-Process -FilePath "git" -ArgumentList $git_args -Wait
    Remove-Item "$templatelocal\.git" -Force -Recurse
    Write-Host "Done`n"
}

foreach ($course in $courses)
{
    #trim spaces
    $course = $course -replace '\s', ''

    if ($course.Length -eq 0)
    {
        continue
    }
    
    Write-Host -NoNewline "Processing course " -ForegroundColor Yellow
    Write-Host $course -ForegroundColor Blue -BackgroundColor Gray

    $en_folder = "$local_root\$course\En-US"
    if (Test-Path $en_folder)
    {
        Remove-Item $en_folder -Force -Recurse
    }
    New-Item -Type Directory -Path $en_folder | Out-Null
    if ($course -match "_en$") 
    {
         $git_args = "clone", "https://$($bitbucket_login)@$($bitbucket_source)/$($course).git", """$en_folder"""
    }
    else
    {
        $git_args = "clone", "https://$($bitbucket_login)@$($bitbucket_source)/$($course)_us_en.git", """$en_folder"""
    }
    Write-Host -NoNewline "`tCloning $course from BucketSource..."
    Start-Process -FilePath "git" -ArgumentList $git_args -Wait
    
    Try
    {
        Remove-Item "$en_folder\.git" -Force -Recurse -ErrorAction Stop
    }
    Catch  [System.IO.IOException]
    {
        
        #Wait 60 seconds for Dropbox to sync first...
        Start-Sleep -Seconds 180
        Remove-Item "$en_folder\.git" -Force -Recurse
        Write-Host "Done`n"
    }

    Write-Host "`tCreating language folders..."
    foreach ($lang in $languages)
    {
        Write-Host -NoNewline "`t`treplicating $en_folder => $local_root\$course\$lang"
        Start-Process -FilePath "robocopy" -ArgumentList "/MIR", """$en_folder""", """$local_root\$course\$lang""" -Wait
        Write-Host "`tDone"
    }

    #Create handoff folder temporarily
    if (-Not (Test-Path "$handoff_folder\$course"))
    {
        New-Item -ItemType Directory -Path "$handoff_folder\$course" | Out-Null
    }

    Write-Host "`tCopy JSON files to Handoff $handoff_folder\$course folder..."
    foreach ($json_file in $json_files)
    {
        Write-Host -NoNewline "`t`tcopying $json_file"
        Copy-Item "$en_folder\en\$json_file" "$handoff_folder\$course\$json_file" -Force
        Write-Host "`tDone"
    }
    Write-Host "`n"
}

# Load assembly for Zip operations
Add-Type -assembly "system.io.compression.filesystem"
$tempFolder = Join-Path ([System.IO.Path]::GetTempPath()) ([System.Guid]::NewGuid())
#Temp folder for new archive
New-Item -ItemType Directory -Path $tempFolder | Out-Null

foreach ($folderToZip in Get-ChildItem $sourceFolder)
{
    Write-Host -NoNewline "`tCopy $folderToZip to $handoffFolder..."
    Copy-Item $folderToZip.FullName -Destination $tempFolder -Recurse
    
    $zipFileName = "$handoffFolder\$folderToZip.zip"
        if (Test-Path $zipFileName)
        {
            Remove-Item $zipFileName -Force
        }
        [System.IO.Compression.ZipFile]::CreateFromDirectory($tempFolder, $zipFileName)
        Get-ChildItem $tempFolder | Remove-Item -Recurse -Force
    
    Write-host "Done"
}

#Remove temp folder
Remove-Item -Path $tempFolder -Recurse -Force
foreach ($project in Get-ChildItem $sourceFolder)
{
    Write-Host -NoNewline "`tMoving $project to Handoff $archive_folder..."
    Copy-Item $project.FullName -Destination $archive_folder -Recurse -Force
    Remove-Item $project.FullName -Recurse -Force
    Write-Host "Done"
}