@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

REM Change these values from HERE ===========
SET DBPath=D:\Dropbox (Facebook)\Blueprint - Localization
SET ScriptPath=d:\scripts\Blueprint\scripts
SET GitUser=pilyi
SET GitPW=i18nIS#1
REM ================================= To HERE
SET TempPath=%scriptPath%\temp
SET TemplatePath=%DBPath%\Handoff\adapt_template_2-0
SET RepoPath=%DBPath%\Handoff\RepoNames
SET /A args=0
SET ProcessType=%1

::Get today's date
FOR /f "tokens=1-2" %%a in ("%date%") do (
  FOR /f "tokens=1-3 delims=/" %%b in ("%%b") do set today=%%d-%%b-%%c
)

::Check argument counts
FOR %%a in (%*) do set /a args+=1

IF {%args%} GEQ {1} (
  IF /i {%1}=={handoff} goto Handoff
	IF /i {%1}=={handback} goto Handback
	IF /i {%1}=={build} goto Build
  IF /i {%1}=={submit} goto Submit
) else (
  goto Usage
)

goto End

:Handoff

IF EXIST "%DBPath%\Handoff\RepoNames\RepoNames.txt" (
	echo.
	%scriptPath%\bin\xecho /s "Running handoff script..." /fg 10 /n
	echo.
	powershell -file "%ScriptPath%\ps1\Handoff.ps1"

	::send out an email
	powershell -file "%ScriptPath%\ps1\email.ps1" %1 "%RepoPath%\RepoNames.txt"

	Goto End
) else (
	echo.
	%scriptPath%\bin\xecho /s "%RepoPath%\RepoNames.txt does not exist" /fg 12 /n
	echo.
)
GOTO End

:Handback
FOR /f %%a in ('dir /b /s "%DBPath%\Handoff\Delivered" ^| find /V /C "::"') do (

  IF not %%a==0  (
    echo.
    %scriptPath%\bin\xecho /s "Running handback script..." /fg 10 /n
    echo.
    powershell -file  "%ScriptPath%\ps1\Handback.ps1"
    REM powershell -file  "%ScriptPath%\ps1\email.ps1"
  ) else (
    echo.
    %scriptPath%\bin\xecho /s "There are no deliveries in %DBPath%\Handoff\Delivered" /fg 12 /n
  )
)

Goto End

:Build

SET hb_list=%2

IF EXIST "%hb_list%" (
  ::Go to the template folderlogPath
  Pushd "%TemplatePath%"

  :: Clean up temp folder under script folder then recreate
  IF exist %TempPath% (
    rd /s /q %TempPath%
    md %TempPath%
  )

  FOR /f "tokens=1-2 delims=\" %%a in ('type "%hb_list%"') do (
    SET language=%%a
    SET RepoName=%%b
    
    ::Clean build folder under adapt_template_2.0
    IF exist "%TemplatePath%\build" rd /s /q "%TemplatePath%\build"

    ::Execute build command
    powershell -file "%ScriptPath%\ps1\BuildLangs.ps1"
  )

  Popd %ScriptPath%
)

::send out an email
powershell -file "%ScriptPath%\ps1\email.ps1" %1 %hb_list%
goto End

:Submit

IF {%2}=={} (
	echo.
	%scriptPath%\bin\xecho /s "Please enter Type..." /n
	%scriptPath%\bin\xecho /s "	example : run.cmd submit D:\test\text.txt" /fg 14 /n
	echo.
	GOTO End
) else (
	for %%a in (%2) do SET "submit_list=%%~dpfnxsa"
)

IF EXIST "%submit_list%" (
  echo.
  %scriptPath%\bin\xecho /s "Running submit script..." /fg 10 /n
  echo.
  powershell -file  "%ScriptPath%\ps1\FinalSubmit.ps1" "%submit_list%"
  ::send out an email
  powershell -file "%ScriptPath%\ps1\email.ps1" %1 "%ScriptPath%\committed.log"
  Goto End
) else (
  echo.
  %scriptPath%\bin\xecho /s "%submit_list% does not EXIST" /fg 12 /n
  echo.
)
Goto End

:Usage
echo.
%scriptPath%\bin\xecho /s "Please enter Type..." /n
%scriptPath%\bin\xecho /s "	example: run.cmd handoff" /fg 14 /n
echo.

:End
